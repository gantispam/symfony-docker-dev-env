#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
    echo "Set apache foreground..."
	set -- apache2-foreground "$@"
fi

if [ "$1" = 'php-run' ] || [ "$1" = 'bin/console' ]; then

    echo "Config environnment : ${SYMFONY_ENV}"
    ln -sf "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

    # echo "Check environment : ${SYMFONY_ENV}"
    # symfony check:requirements

    # The first time volumes are mounted, the project needs to be build
    if [ -f composer.json ] && [ -f bin/console ] && [ ! -d vendor ] && [ ! -d var/cache ]; then

        echo "Create temp folders..."
        mkdir -p var/cache var/logs var/sessions

        echo "Build symfony..."
        composer install --prefer-dist --no-progress --no-suggest --no-interaction
        echo "Composer clear cache"
        composer clear-cache

        echo "Install SQL schema..."
        php bin/console doctrine:schema:update --force

        echo "Install SQL fixtures..."
        php bin/console doctrine:fixtures:load -n

        # echo "Install Assets..."
        # php bin/console assets:install --symlink

        echo "Clear caches..."
        php bin/console cache:warmup
        php bin/console cache:clear

        echo "Fix unix access..."
        chown -R www-data:www-data var
    else
        echo "Symfony instance not found!"
    fi
else
    echo "No symfony instruction found"
fi

echo "Start apache..."
exec apache2-foreground