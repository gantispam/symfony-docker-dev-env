#!/bin/bash

#
# CONSTANTS
#
WORKDIR="/var/www/html"

#
# Create new Symfony project.
#
# @param PRJ_NAME (mandatory)
#
install() {
    PRJ_NAME=$1
    echo "[START] Installation ${PRJ_NAME} on ${WORKDIR}..."

    if [ "x${PRJ_NAME}" = "x" ]; then
        echo "[ERROR] Application's name is missing!"
        exit 1;
    fi
    if [ -f ${WORKDIR}/composer.json ]; then
        echo "[ERROR] Application is already created!"
    fi

    composer create-project symfony/website-skeleton ${PRJ_NAME} && wait \
        && rm -rf ${WORKDIR}/${PRJ_NAME}/.git && wait && sleep 5s \
        && shopt -s dotglob && mv ${WORKDIR}/${PRJ_NAME}/* ${WORKDIR}/ \
        && rm -rf ${WORKDIR}/${PRJ_NAME} \
        && find ./.env -type f -exec sed -i 's/127.0.0.1:3306/sym-database:3306/g' {} \; \
        && yes | composer require symfony/apache-pack \
        && chown -R www-data:www-data var
    echo "[END] Installation..."
}

#
# Remove Symfony project.
#
uninstall() {
    read -p "Are you sure to remove symfony project now (y/n)? " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo -e "\n[START] Uninstall symfony instance..."
        rm -rf ${WORKDIR}/bin
        rm -rf ${WORKDIR}/config
        rm -rf ${WORKDIR}/public
        rm -rf ${WORKDIR}/src
        rm -rf ${WORKDIR}/templates
        rm -rf ${WORKDIR}/test*
        rm -rf ${WORKDIR}/translations
        rm -rf ${WORKDIR}/var
        rm -rf ${WORKDIR}/vendor
        rm -rf ${WORKDIR}/.en*
        rm -rf ${WORKDIR}/composer*
        rm -rf ${WORKDIR}/phpunit*
        rm -rf ${WORKDIR}/symfony*
        echo "[END] Uninstall symfony instance..."
        exit 0;
    fi
    echo -e "\n[INFO] Cancelled. No change apply."
}

#
# Display symfony logs.
#
logs() {
    echo -e "[START] Read Symfony logs..."
    tail -f ${WORKDIR}/var/log/*.log
}


############## Create your custom helper here ##############

############## // Create your custom helper here ##############

echo "[START] Running function $@ on helper..."
"$@"