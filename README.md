<p align="center"><a href="https://symfony.com" target="_blank">
    <img src="https://symfony.com/logos/symfony_black_02.svg">
</a></p>

https://symfony.com is a **PHP framework** for web applications and a set of reusable
**PHP components**. Symfony is used by thousands of web applications (including
BlaBlaCar.com and Spotify.com) and most of the popular PHP projects (including
Drupal and Magento).

# symfony-docker-dev-env
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Simple and quick symfony environment for developer.

*Do not use this containers on production environment!*

## Requirement
* Docker Engine : https://www.docker.com/
* docker-compose plugin : https://docs.docker.com/compose/
* Internet

## Technical stack
* Service "sym-app" :
    * Environment : Debian 10 buster
    * WebServer : PHP 7.2 + Apache
    * WebApp : Symfony (latest stable)
    * Dependencies manager : Composer (latest stable)
    * Debugger : XDebug 2.7.2
* Service "sym-database" :
    * Database : MariaDB 10.3.7 (Mysql Fork)

## How to use?

### Installation

#### CASE 1 - Without existing Symfony project
* Download or clone this repository
* Build environment : 
    ```console
    docker-compose -f docker-compose-dev.yml up --build -d
    ```
* Create new Symfony web-site project
  * OPTION 1 - With helper : 
  ```consoler
  docker exec -ti sym-app helper install my_app_name
  ```
  * OPTION 2 - Without helper :
  ```console
  docker exec -ti sym-app composer create-project symfony/website-skeleton my_project_name
  [...]
  ```

#### CASE 2 - With existing Symfony project (migration case)
* Download or clone this repository
* Merge this repository files on your Symfony projet. Exemple : 
```console
 shopt -s dotglob && mv /my-path/this-repository/* /my-path/my-app-folder/
```
* Build environment : 
```console
cd /my-path/my-app-folder/
docker-compose -f docker-compose-dev.yml up --build -d
```

### Entrypoint
* Apache server : ```http://localhost:80```
* Symfony server : ```http://localhost:8000```
* Xdebug : ```http://localhost:9001``` (config PhpStorm)

## Usage tips
## Use Symfony console
* According with Symfony documentation (https://symfony.com/doc/current/setup.html#running-symfony-applications).<br/>Command line will be :
```console
docker exec -ti sym-app symfony
```

## Use embedded server (Symfony server)
* Default installation use APACHE has webservice (port 80). you can use Symfony embedded server :
```console
docker exec -ti sym-app symfony server:start
```
* Entrypoint : 
  * Web : ```http://localhost:8000```

## Create your custom helper
Helper make your life easier!
  * Add you custom function in file ```environment/dev/helper.sh```<br/>
  WARN : function's name is your helper's name!
  * Build environment :
  ```console
  docker-compose -f docker-compose-dev.yml down -v && docker-compose -f docker-compose-dev.yml up --build -d
  ```
  * Usage : 
  ```console
  docker-compose exec -ti sym-app helper myHelperName
  ```
    
## Observations
* You can access on PHP environment with command line (read https://docs.docker.com/): 
```console
docker exec -ti sym-app bash
```
